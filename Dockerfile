FROM ruby:2.6.1

WORKDIR /usr/src

COPY . .

RUN bundle install

CMD rails server -b 0.0.0.0

