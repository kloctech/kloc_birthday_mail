class CouponCodeMailSender
    def self.sending_mails
      shops = Shop.all
      applicable_customers = []
      shops.each do |shop|
        @shop_setting = shop.setting
        customers_count = shop.setting.num_of_customer.to_i
        shop.customers.each do |customer|
          pre_days = @shop_setting.day_number.to_i
          if(customer.birthday.to_s.length >= 5)
            # binding.pry
            if( customer.birthday.strftime('%m-%d') == ((Date.today + pre_days).strftime('%m-%d')) )
              # binding.pry
              applicable_customers.push(customer)
            elsif( (customer.birthday.strftime('%m-%d') < ((Date.today + pre_days).strftime('%m-%d'))) &&  ( customer.birthday.strftime('%m-%d') > Date.today.strftime("%m-%d") )) 
              #binding.pry
              applicable_customers.push(customer)
              
            end
          end  
        end
        if @shop_setting.num_of_customer == 'all'
          applicable_customers.each do |customer|
            
            @UserMailer.birthday(customer).deliver!
          end   
        else
         selected_customers = applicable_customers.sample(customers_count)
          selected_customers.each do |customer|
            UserMailer.birthday(customer).deliver!
          end
        end
      end  
    end
end

