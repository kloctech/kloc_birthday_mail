var eventName = typeof(Turbolinks) !== 'undefined' ? 'turbolinks:load' : 'DOMContentLoaded';

if (!document.documentElement.hasAttribute("data-turbolinks-preview")) {
  document.addEventListener(eventName, function flash() {
    let a = document.getElementById('shopify-app-flash');
    console.log('is dom node present ?', a);
    console.log('dataset....', a.dataset);
    console.log('flash.....', a.dataset.flash);
    let b = a.dataset.flash;
    console.log('parsing dataset flash.....', JSON.parse(b));
    var flashData = JSON.parse(document.getElementById('shopify-app-flash').dataset.flash);

    if (flashData.notice) {
      ShopifyApp.flashNotice(flashData.notice);
    }

    if (flashData.error) {
      ShopifyApp.flashError(flashData.error);
    }

    document.removeEventListener(eventName, flash)
  });
}
