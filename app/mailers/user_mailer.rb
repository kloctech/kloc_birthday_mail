class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.birthday.subject
  #
  def birthday(customer)
    # binding.pry

    if customer.customer_received_mails.empty?
      CustomerReceivedMail.create(received_mail: true, year_of_send_mal: customer.created_at, customer_id: customer.id)
      @customer = customer
      @shop = @customer.shop
      @message = @shop.setting.email_subject
      @shop_setting = @shop.setting
      @coupon_code = @shop_setting.birthdaycouponcode
      @body = @shop.setting.email_content
      mail to: @customer.customer_email, subject: "#{@message}", content: "#{@body}"
      # binding.pry
    else

    end
 
   
  end
end
