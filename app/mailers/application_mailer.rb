class ApplicationMailer < ActionMailer::Base
  default from: 'birthdaydiscountemail@kloctechnologies.com'
  layout 'mailer'
end
