class Shop < ActiveRecord::Base
  include ShopifyApp::SessionStorage
  has_one :setting
  has_many :customers, dependent: :destroy
  has_many :coupons
  has_one :billing
  has_many :orders
  
  after_create :sync_customer_info

  def api_version
    ShopifyApp.configuration.api_version
  end

  def sync_customer_info
	activating_session_for_latest_version
	@current_shop = ShopifyAPI::Shop.current
	@domain = @current_shop.myshopify_domain
	@shop = Shop.find_by(shopify_domain: @domain)
  	count = ShopifyAPI::Customer.count
    page = 1
	@shop_id = @shop.id 
	while count > 0 do 
		customers = ShopifyAPI::Customer.find(:all,:params => {:limit => 250, :page=> page})
		customers.each do |customer|
		@customer_record = customer
    hash_giver
    @customer_name = customer.first_name
    @customer_email = customer.email
    @customer = Customer.new(shopify_customer_id: customer.id, shop_id: @shop.id, customer_name: @customer_name, customer_email: @customer_email, birthday: @customer_information["birthdate"])
    @customer.save
	end
	count = count - 250
	page = page + 1
	end
  end

  def activating_session_for_latest_version
	shop_url = "https://#{ShopifyApp.configuration.api_key}:#{self.shopify_token}@#{self.shopify_domain}/admin"
	ShopifyAPI::Base.site = shop_url
  end

  def hash_giver
	@customer_information = {}
	if(@customer_record.note)
    if(@customer_record.note.split("\n").count.positive?)
      @customer_record.note.split("\n").each do |customer_string|
        splitter = customer_string.split(":")
        @customer_information[splitter[0].delete_suffix(" ").delete_prefix(" ")] = splitter[1].delete_suffix(" ").delete_prefix(" ")
      end
    end
  end  
	return @customer_information
  end
end
