class Setting < ApplicationRecord
	belongs_to :shop
	validates_uniqueness_of :shop_id
	validates_presence_of :coupon_exp_date, :birthdaycouponcode, :email_subject, :email_content
	validates_numericality_of :coupon_exp_date, :num_of_customer, :day_number, :greater_than_or_equal_to => 1
	validate :assign_all_if_the_customers_field_is_empty,on: [:create, :update]
	validate :non_zero, on: [:create, :update]

	def non_zero
		if self.day_number == 0
		   self.errors.add(:day_number, "Field can't be zero")
		end
	end
	def assign_all_if_the_customers_field_is_empty
	  num_of_customer
	  regex = /\A[+-]?\d+(\.[\d]+)?\z/
	  if num_of_customer == ''
		self.num_of_customer = 'all'  
	  elsif num_of_customer.strip == 'all'
		#do nothing
	  elsif num_of_customer.to_i < 0
	    self.errors.add(:num_of_customer,'can\'t be negative')				
	  elsif(num_of_customer[/\d+/] == nil) 
	    self.errors.add(:num_of_customer,'Invalid Entry in number of customers field')
	  elsif(!!regex.match(num_of_customer))
	  else
		self.errors.add(:num_of_customer,'Invalid Entry in number of customers field') 
	  end		
	end
end  
