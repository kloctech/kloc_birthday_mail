class Customer < ApplicationRecord
	has_one :coupon
  belongs_to :shop
  has_many :orders
  has_many :customer_received_mails, dependent: :delete_all
  # validates_uniqueness_of :shopify_customer_id
  validates_uniqueness_of :customer_email
  validate :correct_date_of_birthday_field, on: :create
  validate :correct_date_of_birthday_field, on: :update


  def self.import(file)

    @shop_id = ApplicationHelper.get_shop.id
    status = {success: false, line:0, record_count: 0}
    CSV.foreach(file.path, headers: true).with_index do |row, index|
      # binding.pry
      if row["customer_email"].class == NilClass || row["birthday"].class == NilClass
        status[:line] = index + 2
        #binding.pry
        #return status
      else
        customer = new
        customer.attributes = row.to_hash
        customer.customer_email = row["customer_email"]
        customer.birthday = row["birthday"]
        customer.shop_id = @shop_id
        # binding.pry
        if customer.save

          if customer.birthday.strftime("%d-%m") == Date.today.strftime("%d-%m")
            #binding.pry
            UserMailer.birthday(customer).deliver!   
          else
            #binding.pry
            CouponCodeMailSender.sending_mails
          end

          status[:record_count] += 1
        else
          # binding.pry
          status[:line] = index + 2
          return status
        end
      end
    end
    status[:success] = true
    return status
  end


  def self.say_hello_to_the_world
    CouponCodeMailSender.new(Shop.first)
  end	

  def correct_date_of_birthday_field
    if(self.birthday > Date.today)
       self.errors.add(:birthday,' The Birthdate is invalid!')   
    end
  end
end
