class Coupon < ApplicationRecord
	belongs_to :customer
	belongs_to :shop
	#validates_numericality_of :setting.day_number, :greater_than_or_equal_to => 1
	validate :non_zero

	def non_zero
		if self.day_number == 0
		   self.errors.add(:day_number, "Field can't be zero")
		end
	end
end
