module ApplicationHelper

  def self.get_shop
    @domain = ShopifyAPI::Shop.current.myshopify_domain
    @shop = Shop.find_by(shopify_domain: @domain)
  end
end
