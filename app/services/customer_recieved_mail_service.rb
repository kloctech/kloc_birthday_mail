class CustomerRecievedMailService
    # binding.pry
    attr_reader :received_mail, :year_of_send_mal, :customer_id
    def initialize customer_id
        # binding.pry
        @received_mail = true
        @year_of_send_mal = Date.today.strftime("%d-%m")
        @customer_id = customer_id
    end

    def create_record
        # binding.pry
        CustomerReceivedMail.create(received_mail: self.received_mail, year_of_send_mal: self.year_of_send_mal, customer_id: self.customer_id)
        # binding.pry
    end
end

