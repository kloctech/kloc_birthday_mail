class ShopifyApp::WebhooksController < ApplicationController
  include ShopifyApp::WebhookVerification
  skip_before_action :verify_authenticity_token, raise: false
  skip_before_action :verify_webhook, raise: false
  include ApplicationHelper

  def receive_customer_create_params
    params.permit!
    head :ok
    @shop = Shop.find_by(shopify_domain: shop_domain)
    params_maker
    birthdate_maker
    @customer = Customer.new(shopify_customer_id: params["id"], customer_name: params["first_name"], customer_email: params["email"], shop_id: @shop.id)
    @customer.birthday = Date.parse(@birthdate) if @birthdate
    # binding.pry
    if(@customer.save)
      if @customer.birthday
        if @customer.birthday.strftime("%d-%m") == Date.today.strftime("%d-%m")
          UserMailer.birthday(@customer).deliver!
        else
        
                      shops = Shop.all
                      applicable_customers = []
                      shops.each do |shop|
                          @shop_setting = shop.setting
                          customers_count = shop.setting.num_of_customer.to_i
                          shop.customers.each do |customer|
                              pre_days = @shop_setting.day_number.to_i
                              if(customer.birthday.to_s.length >= 5)
                                # binding.pry
                                  if( customer.birthday.strftime('%m-%d') == ((Date.today + pre_days).strftime('%m-%d')) )
                                    # binding.pry
                                    applicable_customers.push(customer)
                                  elsif( (customer.birthday.strftime('%m-%d') < ((Date.today + pre_days).strftime('%m-%d'))) &&  ( customer.birthday.strftime('%m-%d') > Date.today.strftime("%m-%d") )) 
                                    #binding.pry
                                    applicable_customers.push(customer)
                                  end
                              end  
                          end
                        if @shop_setting.num_of_customer == 'all'
                          applicable_customers.each do |customer|
                            @UserMailer.birthday(customer).deliver!
                          end   
                        else
                        selected_customers = applicable_customers.sample(customers_count)
                          selected_customers.each do |customer|
                            UserMailer.birthday(customer).deliver!
                          end
                        end
                      end  
        end
      end
    end
  end

  def delete_customer
    begin
      Customer.find_by(shopify_customer_id: params[:id]).destroy!

    rescue ActiveRecord::RecordNotFound
      head :ok
      puts "this is because the customer record is not found"
    end
  end

  # def order_create
  #   params.permit!
  #   head :ok
  #   @shop = Shop.find_by(shopify_domain: shop_domain)
  #   binding.pry
  #   @order = Order.new(shopify_order_id: params["id"], shop_id: @shop.id)
  #   @order.save
  # end


  def birthdate_maker
    @birthdate = nil
    @params["note"].split(",").each do |note_information|
      arr1 = note_information.split(":")
      if(arr1.count > 0)
        if(arr1[0] == "birthdate")
          @birthdate = arr1[1]
        end 
      end 
    end
  end

  def params_maker
    @params = params
  end
end