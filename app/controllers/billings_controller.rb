class BillingsController < ShopifyApp::AuthenticatedController
    def index
        recurring_application_charge = ShopifyAPI::RecurringApplicationCharge.new(
            name: "Premium Plan",
            price: 1,
            return_url: "https://birthday.klocapps.com/check_status_of_charge_activation",
            trail_days: 1,
            test: true,
            terms: "After the free trial Shopify will deduct money from your account as mentioned in the plan"
        )
        recurring_application_charge.save
        @charge = recurring_application_charge.confirmation_url
    end

    def check_status_of_charge_activation
        application_charge = ShopifyAPI::RecurringApplicationCharge.find(params[:charge_id])
        shop = ShopifyAPI::Shop.current
        current_shop = Shop.find_by(shopify_domain: shop.myshopify_domain)
        session[:status] = application_charge.status
        if application_charge.status == "declined" || application_charge.status == "pending"
            redirect_to root_path
        else
            billing = Billing.new
            billing.charge_id = application_charge.id
            billing.status = application_charge.status
            billing.shop_id = current_shop.id
            billing.save
            redirect_to root_path
        end
    end
end
