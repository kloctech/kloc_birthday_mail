class CustomersController < ShopifyApp::AuthenticatedController
  before_action :set_customer, only: [:show, :edit, :update, :destroy]
  layout 'application'
  # skip_before_action :verify_authenticity_token
  # require 'json'
  # GET /customers
  # GET /customers.json
  def index
    @customers = Customer.where(shop_id: ApplicationHelper.get_shop.id)
  end

  # def prevent_from_corb_and_cors
  #   headers['Access-Control-Allow-Origin'] = request.env['HTTP_ORIGIN']
  #   headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
  #   headers['Access-Control-Max-Age'] = '1000'
  #   headers['Access-Control-Allow-Headers'] = '*,x-requested-with'
  # end
 
  # def updateinfo
  #   @data = params
  #   binding.pry
  #   render json: {updated:true,msg:"hi"}, :callback => params[:callback]
  #   prevent_from_corb_and_cors
  # end

  # GET /customers/1
  # GET /customers/1.json

  def import
    if params[:file].class == NilClass
      # binding.pry
      redirect_to customers_path, notice: "Error Encountered, Please select the file"
    else
      status = Customer.import(params[:file])
      if status[:success]
        redirect_to customers_path, notice: "CSV Uploaded Successfully"
      else
        # binding.pry
        redirect_to customers_path, notice: "Error Encountered, Please check the file in row no #{status[:line]}"
      end
    end
  end

  def show
  end

  # GET /customers/new
  def new
    @customer = Customer.new
  end

  # GET /customers/1/edit
  def edit
  end

  # POST /customers
  # POST /customers.json
  def create
    @customer = Customer.new(customer_params)

    respond_to do |format|
      if @customer.save
        UserMailer.birthday(@customer).deliver
        format.html { redirect_to @customer, notice: 'Customer was successfully created.' }
        format.json { render :show, status: :created, location: @customer }
      else
        format.html { render :new }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /customers/1
  # PATCH/PUT /customers/1.json
  def update
    respond_to do |format|
      if @customer.update(customer_params)
        format.html { redirect_to @customer, notice: 'Customer was successfully updated.' }
        format.json { render :show, status: :ok, location: @customer }
      else
        format.html { render :edit }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customers/1
  # DELETE /customers/1.json
  def destroy
    @customer.destroy
    respond_to do |format|
      format.html { redirect_to customers_url, notice: 'Customer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer
      @customer = Customer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def customer_params
      params.require(:customer).permit(:shopify_customer_id, :customer_name, :customer_email, :shop_id, :birthday)
    end
   
end
