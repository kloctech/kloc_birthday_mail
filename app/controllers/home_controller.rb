# frozen_string_literal: true

class HomeController < ShopifyApp::AuthenticatedController  
  layout 'application'
  def index
    # binding.pry
    @webhooks = ShopifyAPI::Webhook.find(:all)
    @customers = Customer.all
    if Billing.count == 0
      session[:status] = nil
    end
    recurring_application_charge = ShopifyAPI::RecurringApplicationCharge.new(
        name: "Premium Plan",
        price: 2.99,
        return_url: "https://birthday.klocapps.com/home/check_status_of_charge_activation",
        trial_days: 15,
        test: true,
        terms: "Once the trial period is over the application will charge accordingly")
    recurring_application_charge.save
    
    @charge = recurring_application_charge.confirmation_url
      # Rails.logger.info @webhooks
  end
  def check_status_of_charge_activation
    application_charge = ShopifyAPI::RecurringApplicationCharge.find(params[:charge_id])
    shop = ShopifyAPI::Shop.current
    current_shop = Shop.find_by(shopify_domain: shop.myshopify_domain)
    session[:status] = application_charge.status
    if application_charge.status == "declined" || application_charge.status == "pending"
        redirect_to root_path
    else
        billing = Billing.new
        billing.charge_id = application_charge.id
        billing.status = application_charge.status
        billing.shop_id = current_shop.id
        billing.save
        redirect_to root_path
    end
end

  
end
