class CheckingsController < ApplicationController
    include ShopifyApp::AppProxyVerification
    def check_customer_dob_present
        #binding.pry
        #params["customer_id"]
        @customer = Customer.find_by(shopify_customer_id: params["customer_id"] )
        #binding.pry
        Rails.logger.info "the birthday is #{@customer.birthday}"
        if((@customer.birthday)&&(@customer.birthday.to_s.length > 5))
            render json: {is_there: true}, :callback => params[:callback]
        else
            render json: {is_there: false}, :callback => params[:callback]
        end
    end

    def check_customer_dob_present_and_update 
        #binding.pry
        #params["customer_id"]
        @customer = Customer.find_by(shopify_customer_id: params["customer_id"] )
        #binding.pry
        @customer.birthday = params["birthdate"]
        if @customer.save
            render json: {is_updated: true}, :callback => params[:callback]
        else
            render json: {is_updated: false}, :callback => params[:callback]
        end
    end
end 