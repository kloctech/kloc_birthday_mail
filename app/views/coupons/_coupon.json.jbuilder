json.extract! coupon, :id, :coupon_code, :shop_id, :is_redeem, :customer_id, :created_at, :updated_at
json.url coupon_url(coupon, format: :json)
