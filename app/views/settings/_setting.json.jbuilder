json.extract! setting, :id, :shop_id, :day_number, :num_of_customer, :coupon_exp_date, :created_at, :updated_at
json.url setting_url(setting, format: :json)
