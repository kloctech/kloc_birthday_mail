json.extract! customer, :id, :shopify_customer_id, :customer_name, :customer_email, :shop_id, :created_at, :updated_at
json.url customer_url(customer, format: :json)
