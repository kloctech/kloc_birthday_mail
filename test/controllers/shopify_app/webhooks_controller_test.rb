require 'test_helper'

class ShopifyApp::WebhooksControllerTest < ActionDispatch::IntegrationTest
  test "should get receive_order_create_params" do
    get shopify_app_webhooks_receive_order_create_params_url
    assert_response :success
  end

end
