ShopifyApp.configure do |config|
  config.application_name = "Birthday Discount Email Made Simple"
  config.api_key = "#{CONFIG[:api_key]}"
  config.secret = "#{CONFIG[:secret]}"
  config.scope = "read_products, read_customers, write_customers" # Consult this page for more scope options:
                                 # https://help.shopify.com/en/api/getting-started/authentication/oauth/scopes
  config.embedded_app = true
  config.after_authenticate_job = false
  config.session_repository = Shop
  config.webhooks = [
    {topic: 'customers/create', address: "#{CONFIG[:whitelisted_url]}/shopify_app/webhooks/receive_customer_create_params", format: 'json'},
    {topic: 'customers/delete', address: "#{CONFIG[:whitelisted_url]}/shopify_app/webhooks/delete_customer", format: 'json'}
    # {topic: 'order/create', address: "#{CONFIG[:whitelisted_uel]}/shopify_app/webhooks/order_create", format: 'json'}
  ]
end
