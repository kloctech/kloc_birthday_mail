env :PATH, ENV['PATH']
# Use this file to easily define all of your cron jobs.
set :environment, "production"
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
 #set :output, "/cron_log.log"
 set :output, "log/cron_log.log"
#
# every 2.days, :at => '12:00 am' do
# #   command "/usr/bin/some_great_command"
# #   runner "MyModel.some_method"
# #   rake "some:great:rake:task"
#     puts 'hello'
#     rake 'email_notification:sending_emails'
#     command "echo 'you can use raw cron syntax too'"
#     #rake "email_notification:everybday"
# end

# every 5.minutes do
#     #   command "/usr/bin/some_great_command"
#     #   runner "MyModel.some_method"
#     #   rake "some:great:rake:task"
#     puts 'hello'
#     #rake 'email_notification:sending_emails'
#     command "echo 'you can use raw cron syntax too'"       
#     #rake "email_notification:everybday"
# end

# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end
# every 24.hours do

every 5.minutes do
  # binding.pry
  rake 'email_notification:sending_emails'
end

# Learn more: http://github.com/javan/whenever
