Rails.application.routes.draw do
  namespace :shopify_app do
    post 'webhooks/receive_customer_create_params'
    post 'webhooks/delete_customer'
    post 'webhooks/order_create'
    
  
  end
  post '/customers/updateinfo',to: "customers#updateinfo"
  resources :coupons
  resources :faq
  resources :customers do
    collection {post :import}
  end
  resources :settings
  root :to => 'home#index'
  get 'home/check_status_of_charge_activation',to: "home#check_status_of_charge_activation"
  get 'billings/check_status_of_charge_activation', to: "billings#check_status_of_charge_activation"
  post '/app_proxy/checkings/check_customer_dob_present', to: "checkings#check_customer_dob_present"
  post '/app_proxy/checkings/check_customer_dob_present_and_update', to: "checkings#check_customer_dob_present_and_update"
  mount ShopifyApp::Engine, at: '/'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end




