class AddBillingsTable < ActiveRecord::Migration[5.2]
  def self.up
    create_table :billings  do |t|
      t.string :charge_id, null: false
      t.string :status, null: false
      t.integer :shop_id, null: false
      t.timestamps
    end

    add_index :billings, :shop_id, unique: true
  end

  def self.down
    drop_table :billings
  end
end


