class ModifyByDefaultBooleanValueToFalseInCustomerRecievedMailsTable < ActiveRecord::Migration[5.2]
  def change
    change_column :customer_received_mails, :received_mail, :boolean, :default => false
  end
end
