class CreateCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :customers do |t|
      t.integer :shopify_customer_id
      t.string :customer_name
      t.string :customer_email
      t.integer :shop_id

      t.timestamps
    end
  end
end
