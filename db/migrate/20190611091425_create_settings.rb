class CreateSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :settings do |t|
      t.integer :shop_id
      t.integer :day_number
      t.integer :num_of_customer
      t.integer :coupon_exp_date

      t.timestamps
    end
  end
end
