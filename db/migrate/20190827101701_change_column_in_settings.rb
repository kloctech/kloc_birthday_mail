class ChangeColumnInSettings < ActiveRecord::Migration[5.2]
  def self.up
    change_column :settings, :num_of_customer, :string
  end
 
  def self.down
    change_column :settings, :num_of_customer, :integer
  end
end
