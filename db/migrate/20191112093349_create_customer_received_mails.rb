class CreateCustomerReceivedMails < ActiveRecord::Migration[5.2]
  def change
    create_table :customer_received_mails do |t|
      t.boolean :received_mail
      t.string :year_of_send_mal
      t.references :customer, foreign_key: true

      t.timestamps
    end
  end
end
