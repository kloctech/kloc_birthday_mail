class Removecolumnfromcustomer < ActiveRecord::Migration[5.2]
  def change
    remove_column :customers, :shopify_order_id
  end
end
