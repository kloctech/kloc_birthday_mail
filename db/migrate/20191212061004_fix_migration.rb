class FixMigration < ActiveRecord::Migration[5.2]
   #forward migration
 def self.up
  add_column :settings, :email_content, :text
end

#reverse migration
def self.down
  remove_column :settings, :email_content
end
end
