class CreateCoupons < ActiveRecord::Migration[5.2]
  def change
    create_table :coupons do |t|
      t.integer :coupon_code
      t.integer :shop_id
      t.boolean :is_redeem
      t.integer :customer_id

      t.timestamps
    end
  end
end
