class AddColumnToCustomers < ActiveRecord::Migration[5.2]
  def change
    add_column :customers, :last_order_placing_date, :string
  end
end
