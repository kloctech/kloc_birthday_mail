# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_12_19_061704) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "billings", force: :cascade do |t|
    t.string "charge_id", null: false
    t.string "status", null: false
    t.integer "shop_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["shop_id"], name: "index_billings_on_shop_id", unique: true
  end

  create_table "coupons", force: :cascade do |t|
    t.integer "coupon_code"
    t.integer "shop_id"
    t.boolean "is_redeem"
    t.integer "customer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customer_received_mails", force: :cascade do |t|
    t.boolean "received_mail", default: false
    t.string "year_of_send_mal"
    t.bigint "customer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_customer_received_mails_on_customer_id"
  end

  create_table "customers", force: :cascade do |t|
    t.string "shopify_customer_id"
    t.string "customer_name"
    t.string "customer_email"
    t.integer "shop_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "birthday"
    t.string "last_order_placing_date"
  end

  create_table "settings", force: :cascade do |t|
    t.integer "shop_id"
    t.integer "day_number"
    t.string "num_of_customer"
    t.integer "coupon_exp_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "birthdaycouponcode"
    t.text "email_subject"
    t.text "email_content"
  end

  create_table "shops", force: :cascade do |t|
    t.string "shopify_domain", null: false
    t.string "shopify_token", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["shopify_domain"], name: "index_shops_on_shopify_domain", unique: true
  end

  add_foreign_key "customer_received_mails", "customers"
end
